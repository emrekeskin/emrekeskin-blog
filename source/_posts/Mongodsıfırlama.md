---
title: Meteor Shortcode#3 - Mongod Sıfırlama
author: Emre Keskin
desc: Mongod ayarını default ayarlarına getirme
---

### Mongod ayarını default ayarlarına getirme işlemi


```

unset MONGO_URL
meteor

```
