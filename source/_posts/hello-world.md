---
title: Merhaba Dünya
date: 2016-10-10 14:49:13
author: Emre Keskin
desc: EmpatiLab Tanitim.
---

### EmpatiLab Tanıtım

2 arkadaş birleştik acaba neler yapabiliriz dedik ve bu işe yaptıklarımızıda paylaşarak başlamaya karar verdik. Kaan blogu açtıktan 1 ay sonra oha blogu açmışsın dese bile galiba birşeyler yazacağız :)

### Burada Yapacağımız İşler

> - Yaptığımız projeleri tanıtmak
> - Open Source destek vermek
> - Kullanığımız teknolojileri anlatmak
> - Bize güzel gelen hayatımızı kolaylaştıran ürünleri sizlere anlatmak



### Uğraştığımız Teknolojiler ve Diller

> - FOREVER Javascript :)
> - Yapay Zeka
> - Meteor
> - C# and SQLServer
> - NoSQL (MongoDB)
> - Google Cloud, Amazon AWS and Digital Oceon
> - ve benzeri teknolojilerin hepsi ile yakından uzaktan ilgileniyoruz. Paylaştığımız yazılarımız ile bunu daha da iyi göreceksiniz...



### Ekip
> - Kaan İnel [Team Lead] [on Facebook](https://www.facebook.com/kaaninel?fref=ts)
> - Emre Keskin [Designer and Meteor Developer ]  [on Facebook](https://www.facebook.com/EmreKeskinMac)
